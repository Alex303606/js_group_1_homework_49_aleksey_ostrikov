var chessBoard = '';

for (var y = 0; y < 8; y++) {
  for (var x = 0; x < 8; x++) {
    if ((x + y) % 2 == 0)
      chessBoard += '◻';
    else
      chessBoard += '◼';
  }
  chessBoard += '\n';
}

console.log(chessBoard);